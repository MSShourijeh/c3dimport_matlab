% 
% This is the main file that reads a c3d file in MATLAB.
%           data - any calculated data from the reconstructed C3D
%               file including marker trajectories  and any calculated angles,
%               moments, powers or GRF data
%           a_data - analog data (often sampled at a higher rate) including
%               force plate data and EMG data that might be collected.
%           fp_info - structure with information about where the corners of
%               the force plates are relative to the vicon coordinate
%               system
%           MP_info - data from the MP file if it exists, inlcuding
%               height and weight etc.
% 
% 
close all; clear all; clc;

addpath([pwd,'\c3d_scripts']); % adds the folder that includes the c3d_scripts to working path

[fname, pname] = uigetfile('*.c3d', 'Select STATIC C3D file');
[data, a_data, fp_info, MP_info] = loadc3dfile([pname, fname]);
data = sort_c3d(a_data);


