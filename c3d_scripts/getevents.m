function Events = getevents(itf)

nIndexLAB = itf.GetParameterIndex('EVENT','LABELS');
nIndexSIDE = itf.GetParameterIndex('EVENT','CONTEXTS');
nIndexTime = itf.GetParameterIndex('EVENT','TIMES');
nEvent = itf.GetParameterLength(nIndexTime);

if nEvent>1
    iii = 1;
    for ii = 0 : nEvent-1 % because numeration begins with 0 (zero based number)
        e = itf.GetParameterValue(nIndexTime,ii);
        if e ~= 0
            time(iii) = e;
            
            switch itf.GetParameterValue(nIndexLAB,iii-1)
                case 'Foot Strike'
                    type(iii) = {'FS'};
                case 'Foot Off'
                    type(iii) = {'FO'};
                case 'Event'
                    type(iii) = {'GR'};
            end
            
            
            switch itf.GetParameterValue(nIndexSIDE,iii-1)
                case 'Left'
                    side(iii) = {'L'};
                case 'Right'
                    side(iii) = {'R'};
                case 'General'
                    side(iii) = {'G'};
            end
            
            iii = iii+1;
        end
    end
    firstframe = itf.GetVideoFrame(0); %when we cut the original acquisition in Nexus
    %the first frame is the initial cut point
    VRate = itf.GetVideoFrameRate;
    AVRatio=itf.GetAnalogVideoRatio;
    fRate=VRate*AVRatio;
    time = time-firstframe/VRate; % now the event times and frames refer to initial cut istant
    [time, ord] = sort(time);
    frame = time*VRate+1; % convert the time event to the frame event
    type = type(ord);
    side = side(ord);
    Events.time = time;
    Events.frame = frame;
    Events.side = side;
    Events.type = type;
    Events.fRate = fRate;
    Events.VRate = VRate;
    
else
    Events = [];
    
end