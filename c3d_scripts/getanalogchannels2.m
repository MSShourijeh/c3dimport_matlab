function ACHANNEL = getanalogchannels2(itf, index1, index2)
% GETANALOGCHANNELS - returns structure with nx1 analog data fields.  The
% returned analog data is scaled with offsets removed, but is in the force plate
% coordinate system.
% 
%   USAGE:  ACHANNEL = getanalogchannels(itf, index1*, index2*)
%           * = not a necessary input
%   INPUTS:
%   itf         = variable name used for COM object
%   index1      = start frame index, all frames if not used as an argument
%   index2      = end frame index, all frames if not used as an argument
%   OUTPUTS:
%   ACHANNEL    = structure with nx1 matrix of analog data fields (+ units)

%   C3D directory contains C3DServer activation and wrapper Matlab functions.
%   This function written by:
%   Matthew R. Walker, MSc. <matthewwalker_1@hotmail.com>
%   Michael J. Rainbow, BS. <Michael_Rainbow@brown.edu>
%   Motion Analysis Lab, Shriners Hospitals for Children, Erie, PA, USA
%   Questions and/or comments are most welcome.  
%   Last Updated by Glen Lichtwark: August 3rd, 2007
%   Created in: MATLAB Version 7.0.1.24704 (R14) Service Pack 1
%               O/S: MS Windows XP Version 5.1 (Build 2600: Service Pack 2)
%   
%   Please retain the author names, and give acknowledgement where necessary.  
%   DISCLAIMER: The use of these functions is at your own risk.  
%   The authors do not assume any responsibility related to the use 
%   of this code, and do not guarantee its correctness. 
%
% This file has been modified by Glen Lichtwark (Griffith University) to
% include calculations for the centre of pressure (COP)
%    

if nargin == 1, 
    index1 = itf.GetVideoFrame(0); % frame start
    index2 = itf.GetVideoFrame(1); % frame end
elseif nargin == 2,
    disp('Error: wrong number of inputs.');
    help getanalogchannels;
    return
end

%  fIndex = itf.GetParameterIndex('ANALOG', 'FORMAT');
%  if fIndex == -1,
%      disp('Sorry, this function does not support your analog data format at this time.');
%      return
%  else,
%     format = itf.GetParameterValue(fIndex, 0);
%     if upper(format(1)) == 'S',
%         disp('Error: only supports VICON unsigned analog data at this time');
%     return
%     else, 
%         disp('C3D file contains unsigned analog data, retrieving data...');
%     end
%  end

 nIndex = itf.GetParameterIndex('ANALOG', 'LABELS');
 nItems = itf.GetParameterLength(nIndex);
 unitIndex = itf.GetParameterIndex('ANALOG', 'UNITS');
 rateIndex = itf.GetParameterIndex('ANALOG', 'RATE');
 usedIndex = itf.GetParameterIndex('FORCE_PLATFORM', 'USED');
 n_used = itf.GetParameterValue(usedIndex,0);
 OriginIndex = itf.GetParameterIndex('FORCE_PLATFORM', 'ORIGIN');
 
 ACHANNEL.Rate = itf.GetParameterValue(rateIndex, 0);
 
 cnum = 1;
for i = 1 : nItems,
    
    channel_name = itf.GetParameterValue(nIndex, i-1);  
    
    % find any labels that are numbers and change to CH1 etc
    if ~isempty(str2num(channel_name(1)))
         channel_name = ['CH_' channel_name];
    end
    
    if isfield(ACHANNEL,channel_name)
        channel_name = [channel_name '_' num2str(cnum)];
        cnum = cnum+1;
    end
    
    % find any spaces
    d = findstr(channel_name, ' ');
    if ~isempty(d)
        channel_name(d) = '_';
    end
    d = findstr(channel_name, ':');
    if ~isempty(d)
        channel_name = channel_name(d+1:end);
    end
    d = findstr(channel_name, '(');
    if ~isempty(d)
        channel_name(d) = [];
    end
    d = findstr(channel_name, ')');
    if ~isempty(d)
        channel_name(d) = [];
    end
        
    % TRY AND KEEP CONSISTENT FORMAT i.e. Fx1, Mx1 and Fx2, Mx2 (not FP1Mx
    % etc)
    d = findstr(channel_name, 'FP');
    if ~isempty(d)
        channel_name(d:d+1) = [];
        channel_name = [channel_name(2:end) channel_name(1)];
    end
    
    newstring = channel_name(1:min(findstr(channel_name, ' '))-1);
    if strmatch(newstring, [], 'exact'),
        newstring = channel_name;
    end      
       
    ACHANNEL.(newstring) = ...
        itf.GetAnalogDataEx(i-1,index1,index2,'1',0,0,'0');
    ACHANNEL.(newstring) = cell2mat(ACHANNEL.(newstring));
    ACHANNEL.units.(newstring) = itf.GetParameterValue(unitIndex, i-1);
end

% calculate the forceplate COP (in the forceplate frame of reference)

if isfield(ACHANNEL,'Fx1')
    if ~isempty(usedIndex) && ~isempty(OriginIndex)
        for i = 1:n_used
            C = itf.GetParameterValue(OriginIndex, ((i-1)*3)+2);
            ACHANNEL.(['COPx' num2str(i)]) = (-(ACHANNEL.(['My' num2str(i)]) - ...
                (C*ACHANNEL.(['Fx' num2str(i)])))./ACHANNEL.(['Fz' num2str(i)]));
            ACHANNEL.(['COPy' num2str(i)]) = ((ACHANNEL.(['Mx' num2str(i)]) + ...
                (C*ACHANNEL.(['Fy' num2str(i)])))./ACHANNEL.(['Fz' num2str(i)]));
            ACHANNEL.(['COPz' num2str(i)]) = zeros(size(ACHANNEL.(['COPy' num2str(i)])));
            ACHANNEL.(['COPx' num2str(i)])(logical(ACHANNEL.(['Fz' num2str(i)])==0)) = 0;
            ACHANNEL.(['COPy' num2str(i)])(logical(ACHANNEL.(['Fz' num2str(i)])==0)) = 0;
        end
    end
end

%--------------------------------------------------------------------------