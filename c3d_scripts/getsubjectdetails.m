 function MP = getsubjectdetails(itf, pname)
% GET3DTARGETS - returns structure containing all of the subject details
% from the associated MP file 
% 
%   USAGE:  MP = getsubjectdetails(itf)
%
%   INPUTS:
%   itf        = variable name used for COM object
%   pname      = directory path that the MP file sits in (defaults to
%   current directory)
%
%   OUTPUTS:
%   MP    = structure with all subject details from MP file
%
%   C3D directory contains C3DServer activation and wrapper Matlab functions.
%   This function written by Glen Lichtwark (Griffith University), but based 
%   on similar functions written by:
%   Matthew R. Walker, MSc. <matthewwalker_1@hotmail.com>
%   Michael J. Rainbow, BS. <Michael_Rainbow@brown.edu>
%   Motion Analysis Lab, Shriners Hospitals for Children, Erie, PA, USA
%   Questions and/or comments are most welcome.  
%   Last Updated by Glen Lichtwark: August 3rd, 2007
%   Created in: MATLAB Version 7.0.1.24704 (R14) Service Pack 1
%               O/S: MS Windows XP Version 5.1 (Build 2600: Service Pack 2)
%   
%   Please retain the author names, and give acknowledgement where necessary.  
%   DISCLAIMER: The use of these functions is at your own risk.  
%   The authors do not assume any responsibility related to the use 
%   of this code, and do not guarantee its correctness. 

if nargin < 2
    pname = cd;
end

nameIndex = itf.GetParameterIndex('SUBJECTS', 'NAMES');
markerIndex = itf.GetParameterIndex('SUBJECTS','MARKER_SETS');

MP.Name = itf.GetParameterValue(nameIndex, 0);
MP.MarkerSet = itf.GetParameterValue(markerIndex, 0);

expected_name = [MP.Name '.mp']; % the MP file should be called this

mp_files = dir(fullfile(pname,'*.mp')); % list all MP files in directory
file_exists = 0;

if ~isempty(mp_files)
    %check to make sure the MP file is in the MP file list
    for i = 1:length(mp_files)
        x = strmatch(expected_name,mp_files(i).name);
        if ~isempty(x)
            file_exists = 1;
        end
    end
    if file_exists > 0
%         if strcmp(MP.MarkerSet, 'PlugInGait') || strcmp(MP.MarkerSet, 'OpenSimModel')
            %load the corresponding MP file, which has the same name as the subject
            %name
            [file_data,dat]= readtext([pname '\' expected_name], ',', '', '', 'empty2NaN');
            str_data = char(file_data{:});
            for i = 1:size(str_data,2); 
                if ~isempty(strfind(str_data(i,:),'='));
                    a = strfind(str_data(i,:),'=');
                    MP.(str_data(i,2:a-2)) = str2num(str_data(i,a+1:end));
                end
            end
%         end
    end
end

%--------------------------------------------------------------------------