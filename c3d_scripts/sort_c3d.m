function data_out = sort_c3d(data)
% function data_out  = sort_c3d(data)
%
% Function to sort data loaded from C3D file with loadc3dfile.m so that
% data is appropriately labelled as a marker, angle, moment etc. This is
% necessary when data is processed using the PiG model, where extra marker
% data representing joint moments, centre etc are made and we want to be
% able to distinguish which is which.
%
% INPUT -
%           data - structure containing data from C3d file (using
%           loadc3dfile.m)
%           pig - assign as plugingait (pig) pig = 1, not plugingait, pig =
%           0 (default pig = 1); --> this affects which markers are counted
%           as calculated markers
%
% OUTPUT - data_out - new structure which has data sorted into new fields
%           depending on whether it is Marker data, Angle data, Moment data
%           etc. eg. data.Marker.ASIS rather than data.ASIS
%
% Written by Glen Lichtwark (Griffith University)
% Updated: Dec, 2008


% load the field names
names = fieldnames(data);

% go through each field name and determine if it is something which can be
% sorted into a specific field reference
for i = 1:length(names)
    
    a = data.(names{i}); % load the data in the field
    
    if length(a) == 1 || ischar(a) % ignore if this is of length 1 or a string
        continue
    end
    
    if ~isempty(strfind(names{i},'Angle')) % find angles
        data.Angles.(names{i}) = a; % add to the new reference field
        data = rmfield(data,names{i}); % remove the original field
    end
    
    if ~isempty(strfind(names{i},'Offset')) % find offsets (the static pose joint angles from BB scripts)
        data.Offset.(names{i}) = a;
        data = rmfield(data,names{i});
    end
    
    if ~isempty(strfind(names{i},'Force')) % find forces
        data.Force.(names{i}) = a;
        data = rmfield(data,names{i});
    end
    
    if ~isempty(strfind(names{i},'Moment')) % find moments
        data.Moment.(names{i}) = a;
        data = rmfield(data,names{i});
    end
    
    if ~isempty(strfind(names{i},'Power')) % find powers
        data.Power.(names{i}) = a;
        data = rmfield(data,names{i});
    end
    
    if ~isempty(strfind(names{i},'GRF')) % find GRFs
        data.GRF.(names{i}) = a;
        data = rmfield(data,names{i});
    end
    
    if ~isempty(strfind(names{i},'Mass')) % find centre of mass fields
        data.COM.(names{i}) = a;
        data = rmfield(data,names{i});
    end

end

% go through new field names. anything that is not a string or length 1
% should be marker data.

names = fieldnames(data);

for i = 1:length(names)
    
    a = data.(names{i});
    
    if length(a) == 1 || ischar(a)
        continue
    elseif strcmp(names{i}(1:2),'U_')
        data = rmfield(data,names{i});
    else data.Markers.(names{i}) = a;
        data = rmfield(data,names{i});
    end
    
end

% output data
data_out = data;

