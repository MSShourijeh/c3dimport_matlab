function [data, a_data, fp_info, MP_info, event_info] = loadc3dfile(file)
% function [data, a_data, fp_info, MP_info] = loadc3dfile(file)
%
% Function to load the data from a c3d file into the structured array data.
% The file may be excluded if you wish to choose it from a windows dialog
% box
%
% INPUT -
%           file - the file path that you wish to load (leave blank to
%               choose from a dialog box)
%
% OUTPUT - all structured arrays containing the following data
%           data - any calculated data from the reconstructed C3D
%               file including marker trajectories  and any calculated angles,
%               moments, powers or GRF data
%           a_data - analog data (often sampled at a higher rate) including
%               force plate data and EMG data that might be collected.
%           fp_info - structure with information about where the corners of
%               the force plates are relative to the vicon coordinate
%               system
%           MP_info - data from the MP file if it exists, inlcuding
%               height and weight etc.
%           event_info - event information (time, frame) if it exists

%
% e.g. [data] = loadc3dfile('c:\data\Session_1\trial001.c3d') will only load the
% data from the C3D file into a structured array called 'data'
%
% [data, a_data] = loadc3dfile() will open a windows dialog box allowing
% the user to find the file they want to open and load the reconstructed
% data into a structured array called 'data' and the analog data into a
% structured array called 'a_data'
%
% Adjusted by Giulia Mantovani (addition of events from file) based on Glen Lichtwark's script

[pname,~,~] = fileparts(file);
warning off
itf = c3dserver;

if nargin == 1
    openc3d(itf,2,file);
else openc3d(itf);
end

try
    data = get3dtargets2(itf, 0);
catch ME
    data = [];
end

if nargout > 1
    try
        a_data = getanalogchannels2(itf);
    catch me
        a_data = [];
    end
end

if nargout > 2
    try
        fp_info = getforceplateinfo(itf);
    catch me
        fp_info = [];
    end
end

if nargout > 3
    try
        MP_info = getsubjectdetails(itf, pname);
    catch me
        MP_info = [];
    end
end

%% Events section
% extract EVENTS
if nargout > 4
    try 
        event_info = getevents(itf);
    catch me
        event_info = [];
    end   
end
%%
closec3d(itf);

clear itf;

